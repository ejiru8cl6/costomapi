sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/Input",
	"sap/m/Button",
	'sap/ui/model/Filter',
	'sap/m/MessageToast'
], function(Control, Input, Button, Filter, MessageToast) {
	"use strict";
	return Control.extend("newtype.sap.generic.Dialog_B2B", {

		metadata: {
			properties: {
				value: {
					type: "string",
					defaultValue: ""
				}
			},
			aggregations: {
				_input: {
					type: "sap.m.Input",
					multiple: false,
					visibility: "hidden"
				}
			},
			events: {}
		},

		init: function() {
			var self = this;
			this.setAggregation("_input", new Input({
				showValueHelp: true,
				valueHelpOnly: true,
				valueHelpRequest: this._onPress.bind(this),
				value: this.getValue()
			}));
		},

		setValue: function(sValue) {
			this.setProperty("value", sValue, true);
		},

		_onPress: function(oEvent) {
			var ctrl = this;
			$.ajax("/ErpApi/api/purma")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);

					if (!ctrl._oDialog) {
						ctrl._oDialog = sap.ui.xmlfragment("newtype.sap.generic.view.Dialog_B2B", ctrl);
					}
					ctrl._oDialog.setModel(oData, "purma");
					                             
					ctrl._oDialog.getBinding("items").filter([]);

					ctrl._oDialog.open();
				});
		},
		//視窗關閉
		handleClose: function(oEvent) {
			var ctrl = this;
			var oContexts = oEvent.getParameter("selectedContexts");
			if (oContexts && oContexts.length) {
				var Ma003 = oContexts.map(function(oContext) {
					return oContext.getObject().Ma003;
				}).join(", ");
				MessageToast.show("Your choice is " + Ma003);
		
				ctrl.setValue(Ma003);
				ctrl.getAggregation("_input").setValue(Ma003);
	
			} else {
				MessageToast.show("No new item was selected.");
			}
		},
		//搜尋
		handleSearch: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oBinding = oEvent.getSource().getBinding("items");
			var oFilterOR = new Filter([
				new Filter("Ma001", sap.ui.model.FilterOperator.Contains, sValue, false),
				new Filter("Ma003", sap.ui.model.FilterOperator.Contains, sValue, false)
			], false);

			oBinding.filter(oFilterOR);
		},

		renderer: function(oRM, oControl) {
			oRM.write("<div");
			oRM.writeControlData(oControl);
			oRM.addClass("DialogB2B");
			oRM.writeClasses();
			oRM.write(">");
			oRM.renderControl(oControl.getAggregation("_input"));
			oRM.write("</div>");
		}
	});
});