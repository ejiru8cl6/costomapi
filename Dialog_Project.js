sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/Input",
	"sap/m/Button",
	'sap/ui/model/Filter',
	'sap/m/MessageToast'
], function(Control, Input, Button, Filter, MessageToast) {
	"use strict";
	return Control.extend("newtype.sap.generic.Dialog_Dept", {

		metadata: {
			properties: {
				value: {
					type: "string",
					defaultValue: ""
				}
			},
			aggregations: {
				_input: {
					type: "sap.m.Input",
					multiple: false,
					visibility: "hidden"
				}
			},
			events: {}
		},

		init: function() {
			this.setAggregation("_input", new Input({
				showValueHelp: true,
				valueHelpOnly: true,
				valueHelpRequest: this._onPress.bind(this),
				value: this.getValue()
			}));
		},

		setValue: function(sValue) {
			this.setProperty("value", sValue, true);
		},

		_onPress: function(oEvent) {
			var ctrl = this;
			$.ajax("/ErpApi/api/cmsnb")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);

					if (!ctrl._oDialog) {
						ctrl._oDialog = sap.ui.xmlfragment("newtype.sap.generic.view.Dialog_Project", ctrl);
					}
					ctrl._oDialog.setModel(oData, "cmsnb");
					                             
					ctrl._oDialog.getBinding("items").filter([]);

					ctrl._oDialog.open();
				});
		},
		//視窗關閉
		handleClose: function(oEvent) {
			var ctrl = this;
			var oContexts = oEvent.getParameter("selectedContexts");
			if (oContexts && oContexts.length) {
				var Nb001 = oContexts.map(function(oContext) {
					return oContext.getObject().Nb001;
				}).join(", ");
				var Nb002 = oContexts.map(function(oContext) {
					return oContext.getObject().Nb002;
				}).join(", ");
				MessageToast.show("Your choice is " + Nb002);
		
				ctrl.setValue(Nb001);
				ctrl.getAggregation("_input").setValue(Nb001);
			} else {
				MessageToast.show("No new item was selected.");
			}
		},
		//搜尋
		handleSearch: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oBinding = oEvent.getSource().getBinding("items");
			var oFilterOR = new Filter([
				new Filter("Nb001", sap.ui.model.FilterOperator.Contains, sValue, false),
				new Filter("Nb002", sap.ui.model.FilterOperator.Contains, sValue, false)
			], false);

			oBinding.filter(oFilterOR);
		},

		renderer: function(oRM, oControl) {
			oRM.write("<div");
			oRM.writeControlData(oControl);
			oRM.addClass("DialogProject");
			oRM.writeClasses();
			oRM.write(">");
			oRM.renderControl(oControl.getAggregation("_input"));
			oRM.write("</div>");
		}
	});
});